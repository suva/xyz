<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Online Blood Bank System</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

		<link rel="stylesheet" href="assets/css/bootstrap.css">  <!--new Enter-->
<!--		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> -->
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">  <!--new Enter-->
        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/main.css">
		
		<link rel="stylesheet" href="assets/css/animate.css">

	
		<script src="assets/js/main.js"></script>
		
        <script src="assets/js/jquery-3.0.0.min.js"></script>

        <script src="assets/js/jquery-1.12.4.js"></script>
        <script src="assets/js/bootstrap.min.js"></script> 
       
				<!--jquery-ui.com-->
				<script
			  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
			  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
			  crossorigin="anonymous"></script>
		<link rel="stylesheet" href="assets/css/jquery-ui.css"/>
        <style>
            body{
                background-color: #E2A941 ;
            }
        </style>
		  

    </head>
<body>
<!--Header Part start-->
	<header class="headsection navbar-fixed-top">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">

                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
           <a class="navbar-brand my_class" href="#">
					<div class="logo">
						<img src = "assets/images/logo.gif"/>
					</div>
					<div class="title">
						<h2><span>Blood</span>Bank</h2>
					</div>
				  </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               <li><a href="index.php"><button type="button" class="btn btn-danger btn_danger_modified">Home</button></a></li>
					<li><a href="information.php"><button type="button" class="btn btn-danger btn_danger_modified">Information Center</button></a></li>

					<li><a href="donate_blood.php"><button type="button" class="btn btn-danger btn_danger_modified">Why Donate Blood</button></a></li>
					<li><a href="php/index.php"><button type="button" class="btn btn-danger btn_danger_modified">Member Registration</button></a></li>
										<!--Member Login Part Start-->
                      <li><a href="php/admin.php"><button type="button" class="btn btn-danger btn_danger_modified">Admin</button></a></li>
						
            </ul>
        </div>
    </div>
</nav>
	</header>
<!--Header Part End-->

<!--Information Part Start-->
<div class="container informationsection">
      <div class="row">
		<h1 class="h1_style1">আপনার দেয়া রক্তই পারে একটি জীবন বাঁচাতে </h1>
		<p class="p_style"style="color: black" style="text-align:adjust"><span style="font-size:30px;color:#a90e12;">সৃষ্টির</span> সেরা জীব হিসেবে মানুষ তার বুদ্ধিবৃত্তির কারণে আর যে কোন প্রাণির চেয়ে এগিয়ে আছে তার মেধা, প্রজ্ঞা, আবেগ, ভাব-ভালোবাসায় । আর এই ভালোবাসার এক দারুণ বহিঃপ্রকাশ রক্ত দান । রক্ত দানে আছে অনেক যুক্তি, বিজ্ঞান সম্মত ব্যাখ্যা। কিন্তু দয়ালু মনই পারে নিজের শরীর থেকে এক ব্যাগ রক্ত অন্যকে দান করতে ।
একটা সময় ব্লগ প্রায় নিয়মিতই চোখে পড়তো রক্ত চেয়ে সাহায্যের আবেদন মূলক পোস্ট । আর সেগুলোকে রাস্তার এ্যাম্বুলেন্সের মতোই জরুরী বিবেচনা করে নির্বাচিত পাতায় নেয়া হতো অন্যদের চোখে সহজে পড়ার জন্যে । সময়ে অনেক কিছু বদলেছে। সোস্যাল মিডিয়ায় ফেসবুকের সহজ ব্যবহার রক্ত চেয়ে সাহায্যমূলক স্ট্যাটাসকে সহজেই শেয়ার করাতে পৌঁছে যেতো অনেকের কাছে খুব দ্রুত এবং সেখানে ব্লগ লিখিয়েদেরকেই দেখা যায় তুলনামূলকভাবে বেশি তৎপর হতে ।
রাষ্ট্র তাঁর প্রয়োজনে অনেক সিদ্ধান্তই নিতে পারে, কিন্তু রক্তের অভাবে তো মরতে পারে না একটি জীবনও ।
বন্ধ থাকতে পারে ফেসবুক...কিন্তু এখনও সচল আছে ব্লগের চাকা । আর তাই এই পোস্টের অবতাড়না ।
একজন ব্লগার, একজন দায়িত্বশীল সচেতন নাগরিক। আপনিও পারেন অবদান রাখতে ...জানিয়ে দিতে পারেন আপনার রক্তের গ্রুপ যেনো প্রয়োজনে পাশে পেতে পারি ।</p>

	<h2 class="head_style">আপনি কেন রক্ত দিবেন ?</h2>
	<p class="p_style"style="color: black">- প্রতি ৩ সেকেন্ডে একজনের রক্তের প্রয়োজন হয়।	</p>
	<p class="p_style"style="color: black">- যাদের রক্তের দরকার হয় তাদের মধ্যে ২০%-ই শিশু।</p>
	<p class="p_style"style="color: black">- রক্ত দান করা ১০০% নিরাপদ।</p>
	<p class="p_style"style="color: black">- একজন ডোনারের এক পাইন্ট রক্ত তিন জন মানুষের জীবন বাঁচাতে পারে।</p>
	<p class="p_style"style="color: black">- আমাদের মোট জনসংখ্যার ৬০% মানুষ রক্ত দানে সক্ষম হলেও প্রকৃত পক্ষে দান করেন মাত্র ৪% মানুষ।</p>
	<p class="p_style"style="color: black">তাই আসুন, রক্ত দিয়ে অসহায় মানুষকে সহায়তা করার জন্য হাত বাড়াই..।</p>

	<h2 class="head_style">আপনার দান করা রক্ত শুদ্ধতো ?</h2>
	<p class="p_style"style="color: black">খালি চোখে আমরা অনেকেই অনেক সুস্থ থাকি । কিন্তু রক্তে বহন করে চলি অনেক রোগে জীবানু যা রক্তের মাধ্যমে চলে যেতে পারে অন্যের দেহে । তাই অন্যকে রক্ত দানের পূর্বে অবশ্যই নিশ্চিত হওয়া উচিত আমাদের রক্ত শুদ্ধ কি না । আর নিজের শরীরের রোগ নির্ণয়ের জন্য রক্ত পরীক্ষা একটা ভালো হাতিয়ার। স্বেচ্ছায় রক্তদানের মাধ্যমে আপনি জানতে পারেন আপনার শরীর রক্তবাহিত মারাত্মক রোগ যেমন-হেপাটাইটিস-বি,এইডস, সিফিলিস ইত্যাদির জীবাণু বহন করছে কিনা।
</p>

	<h2 class="head_style">রক্তদানের নুন্যতম যোগ্যতা</h2>
	<p class="p_style"style="color: black">আপনার বয়স যদি ১৮-৬০ বছরের মধ্যে হয়, ওজন যদি হয় কমপক্ষে ৪৮ কেজি এবং আপনি যদি সুস্থ থাকেন, তাহলেই আপনি প্রতি ৪ মাস পর পর রক্ত দিতে পারেন।
</p>

	<h2 class="head_style">রক্ত দান করে সুস্থ থাকুন</h2>
	<p class="p_style"style="color: black">রক্ত দান স্বাস্থ্যের জন্যে উপকারি। নানান গবেষণার ফলে এটা এখন প্রমাণিত যে, রক্ত দেয়া স্বাস্থের জন্যে উপকারি শুধু নয়, রক্ত দিলে একজন মানুষ মুক্ত থাকতে পারেন বেশ কয়েকটি মারাত্মক রোগের ঝুঁকি থেকে:</p>
	<p class="p_style"style="color: black">হৃদরোগ ও হার্ট এটাকের ঝুঁকি কমে : সিএনএন পরিচালিত এক গবেষণায় দেখা যায়, রক্তে যদি লৌহের পরিমাণ বেশি থাকে তাহলে কোলেস্টেরলের অক্সিডেশনের পরিমাণ বেড়ে যায়, ধমনী ক্ষতিগ্রস্থ হয়, ফলাফল হৃদরোগের ঝুঁকি বৃদ্ধি। নিয়মিত রক্ত দিলে দেহে এই লৌহের পরিমাণ কমে যা হৃদরোগের ঝুঁকিকেও কমিয়ে দেয় কার্যকরীভাবে।</p>
	<p class="p_style"style="color: black">ফ্লোরিডা ব্লাড সার্ভিসের এক জরিপে দেখা গেছে, যারা নিয়মিত রক্ত দেন, তাদের হার্ট এটাকের ঝুঁকি অন্যদের চেয়ে ৮৮ ভাগ কম এবং স্ট্রোকসহ অন্যান্য মারাত্মক হৃদরোগের ঝুঁকি ৩৩ ভাগ কম।
</p>
	<p class="p_style"style="color: black">ক্যান্সারের ঝুঁকি কমে : মিলার-কিস্টোন ব্লাড সেন্টারের এক গবেষণায় দেখা যায়, নিয়মিত রক্ত দিলে ক্যান্সারের ঝুঁকি কমে। বিশেষ করে ফুসফুস, লিভার, কোলন, পাকস্থলী ও গলার ক্যান্সারের ঝুঁকি নিয়মিত রক্তদাতাদের ক্ষেত্রে অনেক কম বলে দেখা গেছে।</p>
	<p class="p_style"style="color: black">প্রাণবন্ততা ও কর্মক্ষমতা বৃদ্ধি : রক্তদান করার সাথে সাথে আমাদের শরীরের মধ্যে অবস্থিত বোন ম্যারো নতুন কণিকা তৈরির জন্যে উদ্দীপ্ত হয়। দান করার মাত্র ৪৮ ঘণ্টার মধ্যেই দেহে রক্তের পরিমাণ স্বাভাবিক হয়ে যায়, আর লোহিত কণিকার ঘাটতি পূরণ হতে সময় লাগে ৪ থেকে ৮ সপ্তাহ। আর এই পুরো প্রক্রিয়া আসলে শরীরের সার্বিক সুস্থতা, প্রাণবন্ততা আর কর্মক্ষমতাকেই বাড়িয়ে দেয়।</p>

	<h2 class="head_style">ইসলাম ধর্মে রক্তদান কি বৈধ ?</h2>
	<p class="p_style"style="color: black">মানুষের দেহে রক্তের প্রয়োজনে রক্ত গ্রহনের যেমন বিকল্প নেই, তেমনি রক্তের চাহিদা পূরণের জন্য রক্ত বিক্রয় বৈধ নয়। তবে বিনা মূল্যে রক্ত না পেলে রোগীর জন্য রক্ত ক্রয় করা বৈধ, কিন্তু এতে বিক্রেতা গুনাহগার হবে। নবী করিম (সা.) বলেছেন, ‘প্রত্যেক রোগের ওষুধ আছে। সুতরাং যখন রোগ অনুযায়ী ওষুধ গ্রহণ করা হয়, তখন আল্লাহর হুকুমে রোগী আরোগ্য লাভ করে।’ (মুসলিম)</p>
	<p class="p_style"style="color: black">বিপন্ন মানুষের মহামূল্যবান জীবনের সর্বোচ্চ নিরাপত্তা নিশ্চিত করতে পবিত্র কোরআনে ঘোষিত হয়েছে, ‘আর কেউ কারও প্রাণ রক্ষা করলে সে যেন পৃথিবীর সমগ্র মানবগোষ্ঠীকে প্রাণে রক্ষা করল।’ (সূরা আল-মায়িদা, আয়াত: ৩২)</p>
	  
	<h2 class="head_style">নিজে সুস্থ থাকুন, ভালো রাখুন সবাইকে</h2>
	<p class="p_style"style="color: black">সুস্থ শরীরের জন্যে সঠিক রক্ত প্রবাহ প্রয়োজন। দুর্বল রক্ত প্রবাহের কারণে দেহের অন্যান্য অংশও ক্ষতিগ্রস্ত হতে পারে যেমন- মস্তিষ্ক, হার্ট, লিভার, কিডনি ও বিভিন্ন অঙ্গপ্রত্যঙ্গ। </p>
	<p class="p_style"style="color: black">আসুন জেনে নেই সাধারণ কিছু কথা যাতে সহজেই রক্ত প্রবাহ বাড়িয়ে নিজেকে সুস্থ রাখা যায় ।</p>
	<p class="p_style"style="color: black">১. প্রতিদিন অন্তত ৩০ মিনিট হাঁটুন । [বাসা থেকে বেরিয়ে কোন কাজের উদ্দেশ্যে হেঁটে রওনা দিতে পারেন অথবা শপিংএ বা বন্ধুদের আড্ডায় হেঁটে যেতে পারেন।]</p>
	<p class="p_style"style="color: black">২.সাধারণ কিছু হাত-পায়ের ব্যায়াম করুন ১৫ মিনিট ।</p>
	<p class="p_style"style="color: black">৩. গরম পানি দিয়ে গোছল করে নিন...এটি আপনার রক্ত ধমনী উন্নত করতে সাহায্য করতে পারে আর রক্ত প্রবাহ বৃদ্ধি করতে পারে ।</p>
	<p class="p_style"style="color: black">৪.গ্রিন টি তে আছে অ্যান্টিঅক্সিডেন্ট উপাদান যা দেহের রক্ত প্রবাহ বৃদ্ধি করতে সক্ষম । তাই একাধিকবার পান করুন গ্রিন টি । এটি তৈরি খুবই সহজ। শুধু এক কাপ/মগ গরম পানিতে একটি টিব্যাগ দিয়েই তৈরী হয়ে যায় গ্রিন টি ।</p>
	  

  </div><!--============(./CONTAINER)====================--> 


<!--Information Part End-->
	
		<script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>
        
		<script src="assets/js/jquery-1.12.4.js"></script> 
		<script src="assets/js/jquery-ui.js"></script>   
</body>
</html>